package org.ewhappcenter.ewhaandroid;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class ProfilesActivity extends ActionBarActivity {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profiles);

        listView = (ListView) findViewById(R.id.listView);
        List<Profile> profiles = new ArrayList<Profile>();
        profiles.add(new Profile("한만종", "01088018572"));
        profiles.add(new Profile("김동희", "01088018572"));
        profiles.add(new Profile("이연주", "01088018572"));
        profiles.add(new Profile("한만종", "01088018572"));
        profiles.add(new Profile("김동희", "01088018572"));
        profiles.add(new Profile("이연주", "01088018572"));
        profiles.add(new Profile("김동희", "01088018572"));
        profiles.add(new Profile("이연주", "01088018572"));
        profiles.add(new Profile("김동희", "01088018572"));
        profiles.add(new Profile("이연주", "01088018572"));
        profiles.add(new Profile("김동희", "01088018572"));
        profiles.add(new Profile("이연주", "01088018572"));
        profiles.add(new Profile("김동희", "01088018572"));
        profiles.add(new Profile("이연주", "01088018572"));
        profiles.add(new Profile("김동희", "01088018572"));
        profiles.add(new Profile("이연주", "01088018572"));
        profiles.add(new Profile("김동희", "01088018572"));
        profiles.add(new Profile("이연주", "01088018572"));


        ProfileListAdapter adapter = new ProfileListAdapter(this, profiles);
        listView.setAdapter(adapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profiles, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
