package org.ewhappcenter.ewhaandroid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by manjong on 15. 4. 2..
 */
public class ProfileListAdapter extends BaseAdapter {


    private Context context;
    private List<Profile> profileList;
    private LayoutInflater inflater;

    public ProfileListAdapter(Context context, List<Profile> profileList) {
        this.context = context;
        this.profileList = profileList;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return profileList.size();
    }

    @Override
    public Object getItem(int position) {
        return profileList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;

        if (view == null) {
             view = inflater.inflate(R.layout.item_profile_list, null, false);
             holder = new ViewHolder();
             holder.ivProfile = (ImageView) view.findViewById(R.id.ivProfile);
             holder.tvName = (TextView) view.findViewById(R.id.tvName);
             holder.tvPhone = (TextView) view.findViewById(R.id.tvPhone);
             view.setTag(holder);
        }else{
            holder = (ViewHolder) view.getTag();
        }

        Profile profile = profileList.get(position);

        holder.tvName.setText(profile.getName());
        holder.tvPhone.setText(profile.getPhone());

        return view;
    }
    
    static class ViewHolder{
        ImageView ivProfile;
        TextView tvName;
        TextView tvPhone;
    }
}