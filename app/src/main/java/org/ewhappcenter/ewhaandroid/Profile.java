package org.ewhappcenter.ewhaandroid;

/**
 * Created by manjong on 15. 4. 2..
 */
public class Profile {


    private String name;
    private String image;
    private String phone;

    public Profile(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
