package org.ewhappcenter.ewhaandroid;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.*;
import android.widget.Button;
import android.widget.TextView;
import com.astuetz.PagerSlidingTabStrip;


public class MainActivity extends ActionBarActivity {

    private Button btnWeb;

    ViewPager pager;
    private MyPagerAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize the ViewPager and set an adapter
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));

        // Bind the tabs to the ViewPager
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setViewPager(pager);

    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.e("Activity 상태", "onStart()");

    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.e("Activity 상태", "onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.e("Activity 상태", "onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.e("Activity 상태", "onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.e("Activity 상태", "onDestroy()");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        private final String[] TITLES = {"Categories", "Home", "Top Paid", "Top Free", "Top Grossing", "Top New Paid",
                "Top New Free", "Trending"};

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int position) {
            return CardFragment.newInstance(position);
        }
    }

    private static class CardFragment extends Fragment {

        private int position;

        CardFragment() {
           // 절대 구현하지도 쓰지도 말야야
        }

        static CardFragment newInstance(int position) {
            CardFragment newFragment = new CardFragment();

            Bundle bundle = new Bundle();
            bundle.putInt("position", position);
            newFragment.setArguments(bundle);

            return newFragment;
        }


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            position = getArguments().getInt("position");

        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//            View layout = super.onCreateView(inflater, container, savedInstanceState);

            View layout = inflater.inflate(R.layout.fragment_card, container, false);
            TextView tvPosition = (TextView) layout.findViewById(R.id.tvPosition);

            tvPosition.setText(Integer.toString(position));

            return layout;
        }
    }
}
